import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import LiveMap from './Home/LiveMap';
import Stations from './Home/Stations';
import Lines from './Home/Lines';
import TransitAgencies from './Home/TransitAgencies';
import Statistics from './Home/Statistics';
import Congestion from './Home/Congestion';
import Planner from './Home/Planner';
import Correlation from './Home/Correlation';

import { MuiThemeProvider } from '@material-ui/core/styles';
import theme from './theme';
import Topbar from '../components/Layout/Topbar';
import Loader from '../components/Utilities/Loader';

import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as ACTIONS from '../actions/actions';

import io from 'socket.io-client';
import User from '../api/User';

class Home extends Component {
	constructor(props){
		super(props);

		let hostname = window.location.hostname;
		let server_address = 'http://'+hostname+':3010';
		let access_token = 'Bearer '+ User.getUser().accessToken;

		if(process.env.NODE_ENV === 'development'){
			server_address = 'http://192.168.43.215:3000'; //'http://158.69.211.15:3010';
			access_token = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjdiODc3Y2ExMWY4ZjM5NGIxZGRjNDM3YTc1NGUyMDBjZDBhYWU1Nzk2OTUxNmQ2NDgxMzBhZjA2YzFiMGFiNzFjZGNlMWVlYzRlYzRkNWQyIn0.eyJhdWQiOiIxIiwianRpIjoiN2I4NzdjYTExZjhmMzk0YjFkZGM0MzdhNzU0ZTIwMGNkMGFhZTU3OTY5NTE2ZDY0ODEzMGFmMDZjMWIwYWI3MWNkY2UxZWVjNGVjNGQ1ZDIiLCJpYXQiOjE1NTU4ODk0OTcsIm5iZiI6MTU1NTg4OTQ5NywiZXhwIjoxNTg3NTExODk3LCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.oCNdBM_mDgue50wlkX_xWAr1qXfH2fIOvtuFD34-aKx8kU3Ot5NA3Vj4gXiOaj2zuNkyF2XL6SwuEd-z4T4agnLS8KcXFo0LTTrHWJfPN5oLR-1HtWM-b-0rH8Am3TrpF_hMzERtlWUtbHoz1EVqIiLK6i9i3j8RGE3xF__X_c2YbGbxo-30HZdvUNQLnriCCyoUHN9Zsj02PSM-rOIqTZkaGUmpPR_LN5kLnuha27zl-1z_jJV3oib8c2y4e15aDret-6PywPl8HoeTsMPy0vMJAYuYaVln27nQH-nR6Yc_zo7bgLaTVTfMcwNbJDWtFvJL0D7tSqVdwlAMgZWz-6KFJCcqCiWYdN369KtABPSNfD60WHI3fZpAGu6ni1k9UpnRS7jfRh3f972Pa0JZ1cIzeutj0cEAF_ezG3vDwIqYWKGLJ2XikMX9xGjbpuuXRQy-KOFnTGCxZ4ZHLEVZgiFNgJf0cvjp95aX0Kd_o7qzaTvgCO40MgvVmszh1RNyiZlHmuqjU1peoE-0dyoIpvFLeG0THRw9tnikuWn6WSW6d1HAkIfCbuIBNcw4B9D2uEQ61oKJj3LCvYo1-1hh7gvko_zOvA47rSYJQ5UnwGNcRbMlPnPmlWOWgPANykB9W9X4Ha8e8OBXycABY3GzzORmfPGxhe0NHDKKnIYs0fY';
		}

		let socket = io(`${server_address}/monitors`, {
			transportOptions: {
				polling: {
					extraHeaders: {
						'Authorization': access_token
					}
				}
			}
		});

		socket.on('connect_failed', function(){
			console.log('SOCKET Connection Failed');
		});
		socket.on('connect', function(){
			console.log('SOCKET Connected');
		});
		socket.on('disconnect', function () {
			console.log('SOCKET Disconnected');
		});
		socket.on('error', function(e) {
			console.log('Socket.io reported a generic error ' + e.message);
		});


		this.props.CONNECT_TO_SOCKET(socket);
	}

	componentDidMount(){

		// remove main loader
		const  el = document.getElementById('loading');

		if(el){
			el.remove();
		}
	}

	componentDidUpdate(){
		setTimeout(() => this.props.TURN_OFF_LOADING_INDICATOR(), 300);
	}

	render() {
		return (
			<div className="home">
				<Router>
					<MuiThemeProvider theme={theme}>
						<div className="home-grid">
							<Topbar title={this.props.title}/>
							<div className="home-container">
								<div className="home-route-container">
									{this.props.loading && <Loader/>}
									<Route path="/" exact component={LiveMap}  />
									<Route path="/congestion" component={Congestion} />
									<Route path="/stations" component={Stations} />
									<Route path="/statistics" component={Statistics} />
									<Route path="/agencies" component={TransitAgencies} />
									<Route path="/lines" component={Lines} />
									<Route path="/planner" component={Planner} />
									<Route path="/correlation" component={Correlation} />
								</div>
							</div>
						</div>

					</MuiThemeProvider>
				</Router>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		title: state.pageTitle,
		loading: state.isLoading,
		socket: state.socket
	};
};

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		'TURN_OFF_LOADING_INDICATOR': ACTIONS.TURN_OFF_LOADING_INDICATOR,
		'TURN_ON_LOADING_INDICATOR': ACTIONS.TURN_ON_LOADING_INDICATOR,
		'CONNECT_TO_SOCKET': ACTIONS.CONNECT_TO_SOCKET
	}, dispatch);
};

Home.propTypes = {
	TURN_OFF_LOADING_INDICATOR : PropTypes.func,
	TURN_ON_LOADING_INDICATOR : PropTypes.func,
	CONNECT_TO_SOCKET : PropTypes.func,
	title: PropTypes.string,
	loading: PropTypes.bool,
	socket: PropTypes.object
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
