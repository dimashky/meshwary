import React, { Component } from 'react';

import Grid from '@material-ui/core/Grid';

import { InlineDatePicker  } from 'material-ui-pickers';
import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';
import Paper from '@material-ui/core/Paper';

import * as ACTIONS from '../../actions/actions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {ResponsiveContainer, AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip}  from 'recharts';

import PropTypes from 'prop-types';

const data = [
	{name: '10:00', req: 100},
	{name: '11:00', req: 150},
	{name: '12:00', req: 20},
	{name: '01:00', req: 300},
	{name: '02:00', req: 175},
	{name: '03:00', req: 50},
	{name: '04:00', req: 40},
];

class Statistics extends Component {
	constructor(props){
		super(props);

		let date = new Date();
		this.state = {selectedDate: date};

		props.TURN_ON_LOADING_INDICATOR();
	}

	componentDidMount(){
		this.props.UPDATE_PAGE_TITLE('Statistics');
	}

	_handleDateChange(date) {
		this.setState({ selectedDate: date });
	}

	render() {
		return (
			<div className="statistics">
				<div style={{padding:10, textAlign: 'center'}}>
					<MuiPickersUtilsProvider utils={DateFnsUtils}>
						<InlineDatePicker
							keyboard
							className="date-statistics"
							style={{color:'white'}}
							variant="outlined"
							value={this.state.selectedDate}
							onChange={this._handleDateChange.bind(this)}
						/>
					</MuiPickersUtilsProvider>
				</div>
				<Grid container spacing={8} className="statistics-paper-container">
					<Grid item xs>
						<Paper className="statistics-paper" square={true}><div>64+</div>NEW USERS</Paper>
					</Grid>
					<Grid item xs>
						<Paper className="statistics-paper" square={true}><div>1045+</div>REQUESTS</Paper>
					</Grid>
					<Grid item xs>
						<Paper className="statistics-paper" square={true}><div>1000+</div>COMMUTERS</Paper>
					</Grid>
					<Grid item xs>
						<Paper className="statistics-paper" square={true}><div>10+</div>BUSES</Paper>
					</Grid>
				</Grid>
				<div>
					<ResponsiveContainer width='100%' height={400}>
						<AreaChart  data={data} margin={{top: 10, right: 10, left: 0, bottom: 0}}>
							<CartesianGrid strokeDasharray="3 3"/>
							<XAxis dataKey="name"/>
							<YAxis/>
							<Tooltip/>
							<Area type='monotone' dataKey='req' stackId="1" stroke='#454955' fill='#0089C6' />
						</AreaChart>
					</ResponsiveContainer>
				</div>
			</div>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		'UPDATE_PAGE_TITLE': ACTIONS.UPDATE_PAGE_TITLE,
		'TURN_OFF_LOADING_INDICATOR': ACTIONS.TURN_OFF_LOADING_INDICATOR,
		'TURN_ON_LOADING_INDICATOR': ACTIONS.TURN_ON_LOADING_INDICATOR
	}, dispatch);
};

Statistics.propTypes = {
	UPDATE_PAGE_TITLE: PropTypes.func,
	TURN_OFF_LOADING_INDICATOR: PropTypes.func,
	TURN_ON_LOADING_INDICATOR: PropTypes.func
};

export default connect(null, mapDispatchToProps)(Statistics);
