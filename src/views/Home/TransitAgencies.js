import React, { Component } from 'react';

import * as ACTIONS from '../../actions/actions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import Grid from '@material-ui/core/Grid';
import AgenciesList from '../../components/Lists/AgenciesList';

import PropTypes from 'prop-types';

class TransitAgencies extends Component {
	constructor(props){
		super(props);

		props.TURN_ON_LOADING_INDICATOR();
	}

	componentDidMount(){
		this.props.UPDATE_PAGE_TITLE('Transit Agencies');
	}

	render() {
		return (
			<div className="transit-agencies">
				<Grid container spacing={16}>
					<Grid item xs={12} style={{'height':'75vh', padding:25}}>
						<AgenciesList/>
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		'UPDATE_PAGE_TITLE': ACTIONS.UPDATE_PAGE_TITLE,
		'TURN_OFF_LOADING_INDICATOR': ACTIONS.TURN_OFF_LOADING_INDICATOR,
		'TURN_ON_LOADING_INDICATOR': ACTIONS.TURN_ON_LOADING_INDICATOR
	}, dispatch);
};

TransitAgencies.propTypes = {
	UPDATE_PAGE_TITLE: PropTypes.func,
	TURN_OFF_LOADING_INDICATOR: PropTypes.func,
	TURN_ON_LOADING_INDICATOR: PropTypes.func
};

export default connect(null, mapDispatchToProps)(TransitAgencies);

