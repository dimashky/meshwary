import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';

import Map from '../../components/Map';

import * as ACTIONS from '../../actions/actions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import PropTypes from 'prop-types';

class Congestion extends Component {

	constructor(props){
		super(props);

		props.TURN_ON_LOADING_INDICATOR();
	}

	componentDidMount(){
		this.props.UPDATE_PAGE_TITLE('Commuters');
	}

	render() {
		return (
			<div className="commuters">
				<Grid container spacing={8}>
					<Grid item xs={12}>
						<Map type="commuters"/>
					</Grid>
				</Grid>
			</div>
		);
	}
}



const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		'UPDATE_PAGE_TITLE': ACTIONS.UPDATE_PAGE_TITLE,
		'TURN_OFF_LOADING_INDICATOR': ACTIONS.TURN_OFF_LOADING_INDICATOR,
		'TURN_ON_LOADING_INDICATOR': ACTIONS.TURN_ON_LOADING_INDICATOR
	}, dispatch);
};

Congestion.propTypes = {
	UPDATE_PAGE_TITLE: PropTypes.func,
	TURN_OFF_LOADING_INDICATOR: PropTypes.func,
	TURN_ON_LOADING_INDICATOR: PropTypes.func
};

export default connect(null, mapDispatchToProps)(Congestion);
