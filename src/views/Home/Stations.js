import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';

import Map from '../../components/Map';
import FilterField from '../../components/Filters/FilterField';
import StationsList from '../../components/Lists/StationsList';
import swal from 'sweetalert2';
import * as ACTIONS from '../../actions/actions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import Stop from '../../api/Stop';
import { withSnackbar } from 'notistack';

class Stations extends Component {
	constructor(props){
		super(props);

		this.state = {
			mapCenter: null,
			stops: {},
			searchValue: '',
		};
		props.TURN_ON_LOADING_INDICATOR();
	}

	componentDidMount(){
		this.props.UPDATE_PAGE_TITLE('Stops');
		this.getData();
	}

	getData(){
		Stop.index()
			.then(res => {
				this.setState({ stops: res });
			})
			.catch(e => {
				let err = e.response ? e.response.data.message.toString() : e.message;
				this.props.enqueueSnackbar(err, {variant: 'error'});
			});
	}

	handleSelectItem(idx){
		let stops = this.state.stops;
		if(idx > -1){
			let e = stops.features[idx];
			this.setState({
				mapCenter: {lat: e.geometry.coordinates[0][0][1], lng: e.geometry.coordinates[0][0][0], zoom: 17}
			});
		}
	}

	handleSearch(value){
		this.setState({searchValue: value});
	}

	async handleSubmit({name, feature}, id = -1){
		let idx = this.state.stops.features.findIndex(e => e.id === id);
		try {
			let stops = this.state.stops;
			if(idx > -1){
				if(!feature){
					stops.features[idx].properties['name'] = name;
					feature = stops.features[idx];
				}
				else{
					stops.features[idx] = feature;
				}
				await Stop.update(stops.features[idx].id, feature);
			}
			else{
				stops.features.push(await Stop.create(feature));
			}

			this.props.enqueueSnackbar('Done', {variant: 'success'});
			this.setState({stops: {...stops}});
		}
		catch (e) {
			let err = e.response ? e.response.data.message.toString() : e.message;
			this.props.enqueueSnackbar(err, {variant: 'error'});
		}
	}

	async handleRemove(id){
		let idx = this.state.stops.features.findIndex(e => e.id === id);
		if(idx < 0){
			alert('ERROR');
			return;
		}
		let{value: willDelete} = await swal.fire({
			title: 'You will remove this stop!',
			type: 'warning',
			showCancelButton: true
		});

		if(!willDelete){
			return;
		}

		let stops = this.state.stops;
		try {
			await Stop.delete(stops.features[idx].id);

			stops.features.splice(idx, 1);
			stops.features = [...stops.features];
			this.props.enqueueSnackbar('Done', {variant: 'success'});
			this.setState({stops: {...stops}});
		}
		catch (e) {
			let err = e.response ? e.response.data.message.toString() : e.message;
			this.props.enqueueSnackbar(err, {variant: 'error'});
		}
	}

	render() {
		let {stops} = this.state;
		return (
			<div className="live-map">
				<Grid container spacing={8}>
					<Grid item lg={3}>
						<FilterField
							handleSearch={this.handleSearch.bind(this)}/>
						<StationsList
							stops={stops.features ? stops.features : []}
							submit={this.handleSubmit.bind(this)}
							remove={this.handleRemove.bind(this)}
							search={this.state.searchValue}
							handleSelectItem={this.handleSelectItem.bind(this)}/>
					</Grid>
					<Grid item xs lg={9}>
						<Map
							mapCenter={this.state.mapCenter}
							stops={this.state.stops}
							submit={this.handleSubmit.bind(this)}
							delete={this.handleRemove.bind(this)}
							type="stops"/>
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		'UPDATE_PAGE_TITLE': ACTIONS.UPDATE_PAGE_TITLE,
		'TURN_OFF_LOADING_INDICATOR': ACTIONS.TURN_OFF_LOADING_INDICATOR,
		'TURN_ON_LOADING_INDICATOR': ACTIONS.TURN_ON_LOADING_INDICATOR
	}, dispatch);
};

Stations.propTypes = {
	UPDATE_PAGE_TITLE: PropTypes.func,
	TURN_OFF_LOADING_INDICATOR: PropTypes.func,
	TURN_ON_LOADING_INDICATOR: PropTypes.func,
	enqueueSnackbar: PropTypes.func
};

export default connect(null, mapDispatchToProps)(withSnackbar(Stations));
