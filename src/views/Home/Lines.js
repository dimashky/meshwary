import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';

import Map from '../../components/Map';
import FilterField from '../../components/Filters/FilterField';

import * as ACTIONS from '../../actions/actions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Hotkeys from 'react-hot-keys';
import PropTypes from 'prop-types';
import swal from 'sweetalert2';
import StationsList from '../../components/Lists/StationsList';
import Stop from '../../api/Stop';
import Line from '../../api/Line';
import { withSnackbar } from 'notistack';

class Lines extends Component {
	constructor(props){
		super(props);
		props.TURN_ON_LOADING_INDICATOR();
		this.history = [];
		this.selectedItem = -1;
		this.state = {
			showedLines: {},
			lines: {},
			stops: {},
			searchValue: ''
		};
	}

	componentDidMount(){
		this.props.UPDATE_PAGE_TITLE('Lines');
		this.getData();
	}

	getData(){
		Line.index()
			.then(lines => {
				this.setState({ lines, showedLines: lines });
			})
			.catch(e => {
				let err = e.response ? e.response.data.message.toString() : e.message;
				this.props.enqueueSnackbar(err, {variant: 'error'});
			});

		Stop.index()
			.then(stops => this.setState({stops}))
			.catch(e => {
				let err = e.response ? e.response.data.message.toString() : e.message;
				this.props.enqueueSnackbar(err, {variant: 'error'});
			});
	}

	handleSelectItem(idx){
		let lines = this.state.lines.features;
		this.selectedItem = idx;
		if(idx > -1){
			let e = lines[idx],
				showedLines = {...this.state.lines};
			showedLines.features = [lines[idx]];
			this.setState({
				mapCenter: {lat: e.geometry.coordinates[0][1], lng: e.geometry.coordinates[0][0], zoom: 16},
				showedLines
			});
		}
		else{
			this.setState({
				showedLines: this.state.lines
			});
		}
	}

	handleSearch(value){
		this.setState({searchValue: value});
	}

	async undo(){
		if(this.history.length <= 0){
			this.props.enqueueSnackbar('No Items in history to undo', {variant: 'info'});
			return;
		}
		let item = this.history[this.history.length-1];

		switch (item.type) {
		case 'DELETE':
			await this.handleSubmit({feature: item.data});
			break;
		case 'UPDATE':
			await this.handleSubmit({feature: item.data}, item.data.id);
			break;
		case 'CREATE':
			await this.handleRemove(item.data.id);
			break;
		}
		let lines = {...this.state.lines};
		let showedLines = {...lines};

		if(this.selectedItem > -1){
			showedLines.features = [lines.features[this.selectedItem]];
		}
		else{
			showedLines.features = [...lines.features];
		}

		this.setState({lines, showedLines});
		this.history.pop();
		this.history.pop();
	}

	async handleSubmit({name, feature}, id = -1){
		let idx = this.state.lines.features.findIndex(e => e.id === id);
		try {
			let features = [...this.state.lines.features],
				lines= {...this.state.lines};
			if(idx > -1){
				this.history.push({data: {...features[idx]}, type: 'UPDATE'});
				if(!feature){
					features[idx].properties['name'] = name;
					feature = features[idx];
				}
				else{
					features[idx] = feature;
				}
				await Line.update(features[idx].id, feature);
			}
			else{
				let new_line = (await Line.create(feature));
				features.push(new_line);
				this.history.push({data: {...new_line}, type: 'CREATE'});
			}
			lines.features = features;
			this.props.enqueueSnackbar('Done', {variant: 'success'});
			this.setState({lines});
		}
		catch (e) {
			let err = e.response ? e.response.data.message.toString() : e.message;
			this.props.enqueueSnackbar(err, {variant: 'error'});
		}
	}

	async handleRemove(id){
		let idx = this.state.lines.features.findIndex(e => e.id === id);
		if(idx < 0){
			this.props.enqueueSnackbar('Item which want to delete not found in the array');
			return;
		}
		let{value: willDelete} = await swal.fire({
			title: 'You will remove this line!',
			type: 'warning',
			showCancelButton: true
		});

		if(!willDelete){
			return;
		}

		try {
			let features = [...this.state.lines.features];
			this.history.push({data: {...features[idx]}, type: 'DELETE'});
			await Line.delete(features[idx].id);
			features.splice(idx, 1);
			this.state.lines.features = features;
			this.props.enqueueSnackbar('Done', {variant: 'success'});
			this.setState({lines: {...this.state.lines}});
		}
		catch (e) {
			let err = e.response ? e.response.data.message.toString() : e.message;
			this.props.enqueueSnackbar(err, {variant: 'error'});
		}
	}

	render() {
		let {lines, stops, searchValue, mapCenter, showedLines} = this.state;
		return (
			<Hotkeys
				keyName="ctrl+z"
				onKeyUp={this.undo.bind(this)}
			>
				<div className="live-map">
					<Grid container spacing={8}>
						<Grid item lg={3}>
							<FilterField
								handleSearch={this.handleSearch.bind(this)}/>
							<StationsList
								stops={lines.features ? lines.features : []}
								submit={this.handleSubmit.bind(this)}
								remove={this.handleRemove.bind(this)}
								search={searchValue}
								handleSelectItem={this.handleSelectItem.bind(this)}/>
						</Grid>
						<Grid item xs lg={9}>
							<Map
								mapCenter={mapCenter}
								lines={showedLines}
								stops={stops}
								delete={this.handleRemove.bind(this)}
								submit={this.handleSubmit.bind(this)}
								type="lines"/>
						</Grid>
					</Grid>
				</div>
			</Hotkeys>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		'UPDATE_PAGE_TITLE': ACTIONS.UPDATE_PAGE_TITLE,
		'TURN_OFF_LOADING_INDICATOR': ACTIONS.TURN_OFF_LOADING_INDICATOR,
		'TURN_ON_LOADING_INDICATOR': ACTIONS.TURN_ON_LOADING_INDICATOR
	}, dispatch);
};

Lines.propTypes = {
	UPDATE_PAGE_TITLE: PropTypes.func,
	TURN_OFF_LOADING_INDICATOR: PropTypes.func,
	TURN_ON_LOADING_INDICATOR: PropTypes.func,
	enqueueSnackbar: PropTypes.func
};

export default connect(null, mapDispatchToProps)(withSnackbar((Lines)));
