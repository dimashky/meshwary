import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Map from '../../components/Map';
import * as ACTIONS from '../../actions/actions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import TripInput from '../../components/Forms/TripInput';
import PlannerAPI from '../../api/Planner';
import { withSnackbar } from 'notistack';
import PathSearchResults from '../../components/Lists/PathSearchResults';
import LinearProgress from '@material-ui/core/LinearProgress';

class Planner extends Component {
	constructor(props){
		super(props);
		let longitude = (localStorage.getItem('longitude') ? parseFloat(localStorage.getItem('longitude')) : 36.2765),
			latitude = (localStorage.getItem('latitude') ? parseFloat(localStorage.getItem('latitude')) : 33.5138);
		let source = [longitude, latitude], destination = [longitude + 0.0001, latitude + 0.0001];

		if(localStorage.getItem('sourceMarker') && localStorage.getItem('destinationMarker') ){
			source = JSON.parse(localStorage.getItem('sourceMarker')).value;
			destination = JSON.parse(localStorage.getItem('destinationMarker')).value;
		}

		this.state = {
			source,
			destination,
			searchResult: null,
			loading: false,
			lines: [],
			stops: [],
			congestion: false,
			type: 'bus'
		};
		props.TURN_ON_LOADING_INDICATOR();
	}

	componentDidMount(){
		this.props.UPDATE_PAGE_TITLE('Planner');
	}

	submitHandler(source, destination, type, congestion){
		source = source.split(',').map(e => parseFloat(e));
		destination = destination.split(',').map(e => parseFloat(e));

		if(source.length !== 2 || destination.length !== 2){
			this.props.enqueueSnackbar('You must have only one separator ","', {variant: 'error'});
			return;
		}

		this.setState({source, destination, type});
		/*
		axios.get('http://localhost:3001/rp?src_lat='+source[1]+
			'&src_lng='+source[0]+
			'&dist_lat='+destination[1]+
			'&dist_lng='+destination[0])
			.then(({data}) => {
				let pointList = data.map(e => [e.lng, e.lat]);
				this.setState({route: pointList});
			})
			.catch(err => {
				this.props.enqueueSnackbar(err.message, {variant: 'error'});
			});
		 */
		this.setState({searchResult: null, lines: [], stops: [], loading: true, congestion});
		PlannerAPI.findPath(source, destination, type, congestion)
			.then(res => {
				this.setState({searchResult: res, lines: res.lines, stops: res.stops, loading: false});
			})
			.catch(err => {
				this.setState({loading: false});
				this.props.enqueueSnackbar(err.message, {variant: 'error'});
			});
	}

	onDragMarker(isSource, lat, lng){
		let new_pos = [lng, lat],
			type = isSource ? 'source': 'destination';
		this.setState({[type]: new_pos, searchResult: null});
		localStorage.setItem((isSource ? 'sourceMarker' : 'destinationMarker'), JSON.stringify({value: new_pos}));
	}

	render() {
		let {source, destination, lines, stops, congestion, searchResult, loading} = this.state;
		return (
			<div className="live-map">
				<Grid container spacing={8}>
					<Grid item lg={3}>
						<TripInput
							source={source[0] +' , '+ source[1]}
							destination={destination[0] +' , '+ destination[1]}
							submitHandler={this.submitHandler.bind(this)}/>
						<br />
						{loading && <LinearProgress color="primary" style={{padding: 5, margin:7}} />}
						<PathSearchResults result={searchResult}/>
					</Grid>
					<Grid item xs lg={9}>
						<Map type="planner"
							lines={lines}
							stops={stops}
							onDragMarker={this.onDragMarker.bind(this)}
							source={source}
							destination={destination}
				      congestion={congestion}
						/>
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		'UPDATE_PAGE_TITLE': ACTIONS.UPDATE_PAGE_TITLE,
		'TURN_OFF_LOADING_INDICATOR': ACTIONS.TURN_OFF_LOADING_INDICATOR,
		'TURN_ON_LOADING_INDICATOR': ACTIONS.TURN_ON_LOADING_INDICATOR
	}, dispatch);
};

Planner.propTypes = {
	UPDATE_PAGE_TITLE: PropTypes.func,
	TURN_OFF_LOADING_INDICATOR: PropTypes.func,
	TURN_ON_LOADING_INDICATOR: PropTypes.func,
	enqueueSnackbar: PropTypes.func
};

export default connect(null, mapDispatchToProps)(withSnackbar(Planner));
