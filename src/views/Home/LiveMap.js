import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import MMap from '../../components/Map';
import FilterField from '../../components/Filters/FilterField';
import FilterVehicle from '../../components/Filters/FiterVehicle';
import VehiclesList from '../../components/Lists/VehiclesList';

import * as ACTIONS from '../../actions/actions';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import PropTypes from 'prop-types';

class LiveMap extends Component {
	constructor(props){
		super(props);

		this.state = {
			vehicles: [],
			updatedVehicle: {},
			searchValue: '',
			mapCenter: undefined,
			congestion: {}
		};
	}

	componentDidMount(){
		this.props.UPDATE_PAGE_TITLE('Tracking Map');
		if(this.props.socket){
			this.initSocket();
		}
	}

	componentDidUpdate(prevProps) {
		if(prevProps.socket !== this.props.socket && this.props.socket){
			this.initSocket();
		}
	}

	initSocket(){
		let socket = this.props.socket;

		/*
		socket.emit('init_map', (data) => {
			this.setState({vehicles : data});
		});
		*/

		socket.on('location_updated', this.handleBusUpdate.bind(this));
		socket.on('congestion_updated', (congestion) => this.setState({congestion}));
	}

	componentWillUnmount(){
		this.props.socket.removeListener('location_updated');
		this.props.socket.removeListener('congestion_updated');
	}

	handleBusUpdate(vehicle){

		let vehicles = this.state.vehicles;

		let idx = vehicles.findIndex(e => e.id === vehicle.id);

		if(idx > -1){
			vehicles[idx] = Object.assign(vehicles[idx], vehicle);
		}
		else{
			vehicles.push(vehicle);
		}

		this.setState({
			vehicles: [...vehicles],
			updatedVehicle: {idx, ...vehicle}
		});
	}

	handleSelectVehicle(vehicle){
		this.setState({
			mapCenter: {lat: vehicle.lat, lng: vehicle.lng, zoom: 16}
		});
	}

	handleSearch(value){
		this.setState({searchValue: value});
	}

	render() {
		let {mapCenter, vehicles, updatedVehicle, searchValue, congestion} = this.state;
		return (
			<div className="live-map">
				<Grid container spacing={8}>
					<Grid item xs={false} lg={3}>
						<Grid container spacing={16}>
							<Grid item xs>
								<FilterField
									handleSearch={this.handleSearch.bind(this)}/>
							</Grid>
						</Grid>
						<VehiclesList
							vehicles={vehicles}
							search={searchValue}
							handleSelectVehicle={this.handleSelectVehicle.bind(this)} />
					</Grid>
					<Grid item xs lg={9}>
						<MMap type="live"
							mapCenter={mapCenter}
							vehicles={vehicles}
					    congestion={congestion}
							updatedVehicle={updatedVehicle} />
					</Grid>
				</Grid>
			</div>
		);
	}
}

const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		'UPDATE_PAGE_TITLE': ACTIONS.UPDATE_PAGE_TITLE,
		'TURN_OFF_LOADING_INDICATOR': ACTIONS.TURN_OFF_LOADING_INDICATOR,
		'TURN_ON_LOADING_INDICATOR': ACTIONS.TURN_ON_LOADING_INDICATOR
	}, dispatch);
};

const mapStateToProps = (state) => {
	return {
		socket: state.socket
	};
};

LiveMap.propTypes = {
	UPDATE_PAGE_TITLE: PropTypes.func,
	TURN_OFF_LOADING_INDICATOR: PropTypes.func,
	TURN_ON_LOADING_INDICATOR: PropTypes.func,
	socket: PropTypes.object,
	map: PropTypes.object
};

export default connect(mapStateToProps, mapDispatchToProps)(LiveMap);
