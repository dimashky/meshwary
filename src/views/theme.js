import { createMuiTheme } from '@material-ui/core/styles';

let theme = createMuiTheme({
	palette: {
		type: 'dark',
		primary: {
			main: '#0089C6'
		},
		secondary: {
			main: '#454955'
		},
		error: {
			main: '#D33E43'
		},
		text: {
			main: '#fff'
		}
	},
	typography: {
		'fontFamily': '"Dubai", "Helvetica", "Arial", sans-serif',
		'fontSize': 14,
		'fontWeightLight': 300,
		'fontWeightRegular': 400,
		'fontWeightMedium': 500,
		useNextVariants: true,
	}
});

export default theme;
