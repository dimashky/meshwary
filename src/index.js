import React from 'react';
import ReactDOM from 'react-dom';
import './assets/sass/index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import store from './store/store.js';
import { SnackbarProvider } from 'notistack';
import Cookies from 'js-cookie';
import User from './api/User';
import Swal from 'sweetalert2';

async function init(){
	let access_token = Cookies.get('access_token');
	let current_user = await User.checkAccessToken(access_token);

	if(!access_token || !current_user){
		const {value: user} = await Swal.fire({
			title: 'User Authentication',
			text: 'Enter Username and Password here.',
			showLoaderOnConfirm: true,
			allowOutsideClick: false,
			html:
				'<div>Enter Username and Password here.</div>'+
				'<input id="user-email" required style="margin: 4px" placeholder="Email" type="email" class="swal2-input">' +
				'<input id="user-password" required style="margin: 4px" placeholder="Password" type="password" class="swal2-input">',
			preConfirm: function () {
				let email = document.getElementById('user-email').value,
					password = document.getElementById('user-password').value;
				if(!email || !password){
					Swal.showValidationMessage('Please Fill all fields above.');
					return false;
				}
				Swal.resetValidationMessage();
				return new Promise(function (resolve) {
					User.login(email, password)
						.then(user => resolve(user))
						.catch((err) => {Swal.hideLoading();Swal.showValidationMessage(err.response ? err.response.data.message: err.message);});
				});
			},
			onOpen: function () {
				document.getElementById('user-email').focus();
			}
		});
		User.setUser(user, user.accessToken);
	}
	else{
		User.setUser(current_user, access_token);
	}

	ReactDOM.render(
		<Provider store={store}>
			<SnackbarProvider maxSnack={3}>
				<App />
			</SnackbarProvider>
		</Provider>
		, document.getElementById('root'));
}

init();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();


