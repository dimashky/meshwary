import React, {Component} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Typography from '@material-ui/core/Typography';
import { Scrollbars } from 'react-custom-scrollbars';
import BusIcon from '../../assets/images/icons/Bus.js';
import HippieVanIcon from '../../assets/images/icons/HippieVan.js';
import axios from 'axios';

class LinesList extends Component{
	constructor(props){
		super(props);
		this.state = {
			lines: []
		};
	}

	componentDidMount(){
		axios.get('/data/lines.geojson')
			.then(res => {
				const lines = res.data.features.map(e => e.properties);
				this.setState({ lines });
			});
	}

	_renderList(){
		let jsx = [], lines = this.state.lines;

		lines.forEach((e, idx) => {
			jsx.push(
				<ListItem className="list-item" key={idx}>
					<ListItemAvatar>
						<Avatar className="vehicle-number">
							{e.type === 'bus' ? <BusIcon /> : <HippieVanIcon/>}
						</Avatar>
					</ListItemAvatar>

					<ListItemText className="list-item-text">
						<Typography variant="subtitle1">
							{e.name}
						</Typography>
					</ListItemText>
				</ListItem>
			);
		});

		return jsx;
	}
	render() {
		return(
			<Scrollbars className="vehicles-list-container">
				<List dense={true} className="vehicles-list">
					{this._renderList()}
				</List>
			</Scrollbars>
		);
	}
}

export default LinesList;
