import React from 'react';
import propTypes from 'prop-types';
import { Scrollbars } from 'react-custom-scrollbars';
import TransferWithinAStation from '@material-ui/icons/TransferWithinAStationOutlined';
import TransitEnterexitRounded from '@material-ui/icons/TransitEnterexitOutlined';

class PathSearchResults extends React.Component {
	constructor(props) {
		super(props);
	}

	renderPath(path){
		let jsx = [];
		path.forEach((p,idx) => {
			jsx.push(<li key={idx}>{p.type === -1 ? <TransferWithinAStation/> : <TransitEnterexitRounded/>}{p.value}</li>);
		});
		return jsx;
	}

	render() {
		if (!this.props.result) {
			return [];
		}
		let {length, cost, path, start, end, time, congestion_time} = this.props.result;
		return [
			<div key="meta" className="planner-result-container">
				{!!start && !!start.name && <div>موقف البداية: {start.name}</div>}
				{!!end && !!end.name && <div>موقف النهاية: {end.name}</div>}
				{!!cost && <div>الكلفة: {parseInt(cost)}</div>}
				{!!length && <div>المسافة: {length.toFixed(2)} كم</div>}
				{!!time && <div>الوقت: {`${Math.floor(time) ? `${(Math.floor(time))}  ساعة و `:'' } ${(Math.ceil(Math.round((time%1)*60)))} دقيقة`} </div>}
				{!!congestion_time && <div>الوقت في حال احتساب الازدحام: {`${Math.floor(congestion_time) ? `${(Math.floor(congestion_time))}  ساعة و `:'' } ${(Math.ceil(Math.round((congestion_time%1)*60)))} دقيقة`} </div>}
				{Array.isArray(path) && path.length > 0 && <div>الخطوات:</div>}
			</div>,
			<Scrollbars key="path" className="planner-result-scroller">
				<ol className="list">
					{Array.isArray(path) && path.length > 0 && this.renderPath(path)}
				</ol>
			</Scrollbars>
		];
	}
}

PathSearchResults.propTypes = {
	result: propTypes.object,
};

export default PathSearchResults;
