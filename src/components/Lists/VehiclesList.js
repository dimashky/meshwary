import React, {Component} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Typography from '@material-ui/core/Typography';
import { Scrollbars } from 'react-custom-scrollbars';
import axios from 'axios';
import BusIcon from '../../assets/images/icons/Bus.js';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';

class VehiclesList extends Component{

	constructor(props){
		super(props);
		this.state = {};
	}

	_renderList(){
		let jsx = [], vehicles = this.props.vehicles, search = this.props.search;

		vehicles.forEach((e, idx) => {
			if(!e || !e.name || !e.id){
				return;
			}

			if(search && !(e.name).includes(search) && !(e.id).toString().includes(search)){
				return;
			}

			jsx.push(
				<ListItem className="list-item" key={idx}>
					<Grid container spacing={8}>
						<Grid item xs={12} className="list-item-text">
							<Typography variant="subtitle1">
								<span className="list-item-title" onClick={() => this.props.handleSelectVehicle(e)}>{e.name} ({e.id})</span>
							</Typography>
						</Grid>
					</Grid>
				</ListItem>
			);
		});

		return jsx;
	}

	render() {
		return(
			<Scrollbars className="vehicles-list-container">
				<List dense={true} className="vehicles-list">
					{this._renderList()}
				</List>
			</Scrollbars>
		);
	}
}

VehiclesList.propTypes = {
	vehicles: PropTypes.array,
	handleSelectVehicle: PropTypes.func,
	search: PropTypes.string,
};

export default VehiclesList;
