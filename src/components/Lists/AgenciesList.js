import React, {Component} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Typography from '@material-ui/core/Typography';
import { Scrollbars } from 'react-custom-scrollbars';
import axios from 'axios';
import BusIcon from '../../assets/images/icons/Bus.js';
import TaxiIcon from '../../assets/images/icons/FrontalTaxiCab';
import HippieVanIcon from '../../assets/images/icons/HippieVan.js';

class AgenciesList extends Component{

	constructor(props){
		super(props);
		this.state = {
			agencies: []
		};
	}

	componentDidMount(){
		axios.get('/data/agencies.json')
			.then(res => {
				const agencies = res.data.data;
				this.setState({ agencies });
			});
	}

	_renderList(){
		let jsx = [], agencies = this.state.agencies;

		agencies.forEach((e, idx) => {
			jsx.push(
				<ListItem className="list-item" key={idx}>
					<ListItemText className="list-item-text">
						<Typography variant="h6">
							{e.name}
						</Typography>
					</ListItemText>
					<ListItemIcon className="list-icon">
						{e.type === 'bus' && <BusIcon/> }
						{e.type === 'taxi' && <TaxiIcon/> }
					</ListItemIcon>
				</ListItem>
			);
		});

		return jsx;
	}

	render() {
		return(
			<Scrollbars className="std-list-container">
				<List dense={true} className="std-list">
					{this._renderList()}
				</List>
			</Scrollbars>
		);
	}
}

export default AgenciesList;
