import React, {Component} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import { Scrollbars } from 'react-custom-scrollbars';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField/TextField';
import Send from '@material-ui/icons/Send';
import Delete from '@material-ui/icons/Delete';
import PropTypes from 'prop-types';

class StationsList extends Component{
	constructor(props){
		super(props);
		this.state = {
			stops: (props.stops ? [...props.stops] : []),
			selected_item: '',
			edited_text: ''
		};
	}

	componentDidUpdate(prevProps){
		if(prevProps.stops !== this.props.stops && this.props.stops){
			this.setState({stops: this.props.stops, selected_item: ''});
		}
	}

	handleSelect(idx){
		this.props.handleSelectItem(idx);
		this.setState({
			selected_item: idx,
			edited_text: (idx >= 0 ? this.state.stops[idx].properties.name : '')
		});
	}

	_renderList(){
		let jsx = [
				<div key={-1}>
					<ListItem className="list-item" key={-1}>
						<ListItemText className="list-item-text">
							<Typography variant="subtitle1">
								<span className="list-item-title" onClick={() => this.handleSelect(-1)}>ALL</span>
							</Typography>
						</ListItemText>
					</ListItem>
				</div>
			],
			selected_item = this.state.selected_item,
			search = this.props.search,
			stops = this.state.stops,
			edited_text = this.state.edited_text;

		stops.forEach((e, idx) => {

			if(!e || !e.properties.name || (search && !e.properties.name.includes(search))){
				return;
			}

			let content = <Typography variant="subtitle1">
				<span className="list-item-title" onClick={() => this.handleSelect(idx)}>{e.properties.name}</span>
			</Typography>;

			if(selected_item === idx){
				content = <TextField
					label="Stop Name"
					name="name"
					style={{ margin: 0 }}
					placeholder="Enter the stop name here."
					fullWidth
					margin="normal"
					value={edited_text}
					InputLabelProps={{
						shrink: true,
					}}
					inputProps={{dir: 'auto'}}
					InputProps={{
						endAdornment: (
							<InputAdornment position="end">
								<IconButton
									aria-label="Toggle password visibility"
									onClick={() => this.props.submit({name: edited_text}, e.id)}
								>
									<Send color="primary"/>
								</IconButton>
							</InputAdornment>
						),
						startAdornment: (
							<InputAdornment position="start">
								<IconButton
									aria-label="Toggle password visibility"
									onClick={() => this.props.remove(e.id)}
								>
									<Delete color="error" style={{ fontSize: 20 }}/>
								</IconButton>
							</InputAdornment>
						),
					}}
					onChange={(event) => {
						const { target: { value } } = event;
						this.setState({ edited_text: value });
					}
					}
				/>;
			}

			jsx.push(
				<div key={idx}>
					<ListItem className="list-item" key={e}>
						<ListItemText className="list-item-text">
							{content}
						</ListItemText>
					</ListItem>
				</div>
			);
		});

		return jsx;
	}

	render() {
		return(
			<Scrollbars className="vehicles-list-container">
				<List dense={true} className="vehicles-list">
					{this._renderList()}
				</List>
			</Scrollbars>
		);
	}
}

StationsList.propTypes = {
	search: PropTypes.string,
	handleSelectItem: PropTypes.func,
	stops: PropTypes.array,
	remove: PropTypes.func.isRequired,
	submit: PropTypes.func.isRequired
};

export default StationsList;
