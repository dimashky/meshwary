import React from 'react';
import PropTypes from 'prop-types';
import swal from 'sweetalert2';
import '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css';
const MapboxDraw = require('@mapbox/mapbox-gl-draw');

class StopsMap extends React.Component {

	constructor(props){
		super(props);

		let map = this.props.map;

		this.edited_items = {};
		const draw = new MapboxDraw({
			displayControlsDefault: false,
			controls: {
				polygon: true,
				trash: true
			}
		});

		this.state = {
			hoveredFeature: null,
			data: null,
			draw
		};

		map.addControl(draw, 'top-right');
		map.on('load', () => this.createStopsLayer());
		map.on('draw.create', ({features}) => this.createNewStop(features[0]));
		map.on('draw.update', ({features}) => this.edited_items = features[0]);
		map.on('draw.selectionchange', ({features}) => this.updateStop(features[0]));
		map.on('draw.delete', ({features}) => this.deleteStop(features[0]));
	}

	async createNewStop(feature){

		const {value: stopName} = await swal.fire({
			title: 'Bus Stop Name',
			allowOutsideClick: false,
			input: 'text',
			showCancelButton: false,
			inputValidator: (value) => {
				if (!value) {
					return 'You need to write something!';
				}
			}
		});

		if (!stopName) {
			return;
		}
		feature.properties['name']  = stopName;
		this.props.submit({name: stopName, feature});
	}

	updateStop(feature){
		if(feature || !this.edited_items.id){
			return;
		}

		let idx = this.props.stops.features.findIndex(e => e.id === this.edited_items.id);

		this.edited_items.properties['name'] = this.props.stops.features[idx].properties['name'];
		this.props.submit({feature: {...this.edited_items}}, this.edited_items.id);
		this.edited_items = {};
	}

	deleteStop(feature){
		let idx = this.props.stops.features.findIndex(e => e.id === feature.id);
		this.props.delete(feature.id);
	}

	componentDidUpdate(prevProps){
		let {stops} = this.props;
		if(prevProps.stops !== stops && stops){
			this.state.draw.deleteAll();
			try{
				this.state.draw.add(stops);
			}
			catch (e) {
				console.error(e.message);
			}
		}
	}

	createStopsLayer(){
		let data= {};

		if(this.props.stops){
			data = this.props.stops;
		}

		this.state.draw.deleteAll();
		try{
			this.state.draw.add(data);
		}
		catch (e) {
			console.error(e.message);
		}
	}

	_onHover(event) {
		const {features, srcEvent: {offsetX, offsetY}} = event;
		const hoveredFeature = features && features.find(f => f.layer.id === 'data');

		this.setState({hoveredFeature, x: offsetX, y: offsetY});
	}

	_renderTooltip() {
		const {hoveredFeature, x, y} = this.state;

		return hoveredFeature && (
			<div className="tooltip" style={{left: x, top: y}}>
				<div>{hoveredFeature.properties.name}</div>
			</div>
		);
	}

	render() {
		return [];
	}
}

StopsMap.propTypes = {
	map: PropTypes.object,
	stops: PropTypes.object,
	submit: PropTypes.func,
	delete: PropTypes.func
};

export default StopsMap;
