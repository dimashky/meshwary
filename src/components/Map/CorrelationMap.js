import React from 'react';
import PropTypes from 'prop-types';
import Congestion from '../../api/Congestion';
import moment from 'moment';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import mapboxgl from 'mapbox-gl';

class CorrelationMap extends React.Component {

	constructor(props) {
		super(props);

		let map = this.props.map;

		this.state = {
			hoveredFeature: null,
			data: null
		};

		map.on('load', () => this.getCorrelatedWays());
	}

	getCorrelatedWays() {
		let map = this.props.map;

		try {
			map.removeLayer('ways');
			map.removeSource('ways');
		} catch (e) {
			console.log(e);
		}

		Congestion.correlatedWays()
			.then(res => {
				map.addSource('ways', {
					'type': 'geojson',
					'data': res
				});

				map.addLayer({
					'id': 'ways',
					'type': 'line',
					'source': 'ways',
					'paint': {
						'line-color': '#0089C6',
						'line-width': 4
					}
				});
				map.on('click', 'ways', (e) => {
					const features = map.queryRenderedFeatures(e.point);
					if (features.length > 0) {
						this.getCorrelatedWay(features[0].properties.id);
					}
				});

				map.on('mouseenter', 'ways', function (e) {
					map.getCanvas().style.cursor = 'pointer';
				});

				map.on('mouseleave', 'ways', function () {
					map.getCanvas().style.cursor = '';
				});
			})
			.catch(err => {
				alert(err.message);
			});
	}

	getCorrelatedWay(way_id) {
		let map = this.props.map;

		try {
			map.removeLayer('correlation');
			map.removeSource('correlation');
		} catch (e) {
			console.log(e);
		}

		Congestion.correlatedWays(way_id)
			.then(res => {
				map.addSource('correlation', {
					'type': 'geojson',
					'data': res
				});

				map.addLayer({
					'id': 'correlation',
					'type': 'line',
					'source': 'correlation',
					'paint': {
						'line-color': '#e32636',
						'line-width': 5
					}
				});
			})
			.catch(err => {
				alert(err.message);
			});
	}

	handleReset() {
		const map = this.props.map;
		try {
			map.removeLayer('correlation');
			map.removeSource('correlation');
		} catch (e) {
			console.log(e);
		}
	}

	render() {
		return ( < div className = "map-congestion-panel" >
			<
				div >
				<
					Button variant = "contained"
					color = "primary"
					onClick = {
						this.handleReset.bind(this)
					} >
			Reset < /Button> </div > < /div>
		);
	}
}

CorrelationMap.propTypes = {
	map: PropTypes.object
};

export default CorrelationMap;