import React from 'react';
import PropTypes from 'prop-types';
import mapboxgl from 'mapbox-gl';
import moment from 'moment';
const MapboxDraw = require('@mapbox/mapbox-gl-draw');

class Live extends React.Component {

	constructor(props){
		super(props);

		this.features = [];
		this.idxMap = new Map();

		this.vehicles_markers = [];
		props.map.addControl(new MapboxDraw({
			displayControlsDefault: false,
		}), 'top-right');
	}

	componentDidUpdate(prevProps){
		if(prevProps.updatedVehicle !== this.props.updatedVehicle && this.props.updatedVehicle){
			this.handleUpdateSingleVehicle(this.props.updatedVehicle);
		}

		if(this.props.vehicles && prevProps.vehicles !== this.props.vehicles){
			this.handleUpdateVehiclesList();
		}

		if(this.props.congestion && prevProps.congestion !== this.props.congestion){
			this.handleCongestionUpdate(this.props.congestion);
		}
	}

	handleCongestionUpdate(congestion){
		if(!congestion['features'] || congestion['features'].length === 0){
			return;
		}
		const map = this.props.map;
		try {
			const serverFeatures = congestion['features'];

			serverFeatures.forEach(feature => {
				feature.properties.date = moment();
				const id = feature.properties.id;
				const idx = this.idxMap.get(id);
				if(!idx){
					this.idxMap.set(id, feature.length);
					this.features.push(feature);
				}
				else{
					this.features[idx] = feature;
				}
			});

			try{
				map.removeLayer('congestion');
				map.removeSource('congestion');
			}catch (e) {
				console.log(e);
			}

			congestion.features = this.features;

			map.addSource('congestion', {
				'type': 'geojson',
				'data': congestion
			});

			map.addLayer({
				'id': 'congestion',
				'type': 'line',
				'source': 'congestion',
				'paint': {
					'line-color':['get','color'],
					'line-width': 4
				}
			});
			map.on('click','congestion', (e) => {
				const features = map.queryRenderedFeatures(e.point);
				if(features.length > 0){
					var coordinates = features[0].geometry.coordinates.slice();
					var speed = parseFloat(features[0].properties.speed).toFixed(3),
						name = features[0].properties.name,
						date = moment(features[0].properties.date),
						id = features[0].properties.id,
						historical = parseFloat(features[0].properties.historical).toFixed(3);
					var description = `<h3>${name} ${id}</h3><p>Speed: ${speed} Km/h</p><p>Predicted: ${historical} Km/h</p><p>${date.fromNow()}</p>`;
					try {
						new mapboxgl.Popup()
							.setLngLat(coordinates[Math.floor((coordinates.length)/2)])
							.setHTML(description)
							.addTo(map);
					}
					catch (e) {
						console.error(e);
					}
				}
			});
		}
		catch (e) {
			console.error(e);
		}
	}

	handleUpdateVehiclesList(){
		let vehicles = this.props.vehicles;

		this.vehicles_markers.forEach(marker => {
			marker.remove();
		});
		let vehicles_markers = Array(vehicles.length);

		vehicles.forEach((vehicle, index) => {
			vehicles_markers[index] = this.createNewBusMarker(this.props.map, vehicle);
		});

		this.vehicles_markers= vehicles_markers;
	}

	handleUpdateSingleVehicle(updated_vehicle){
		let vehicles_markers = this.vehicles_markers,
			marker = {};

		if(updated_vehicle.idx === -1){
			marker = this.createNewBusMarker(this.props.map, updated_vehicle);
			vehicles_markers.push(marker);
			marker.remove();
		}
		else{
			marker = vehicles_markers[updated_vehicle.idx];
			marker.setLngLat([updated_vehicle.lng, updated_vehicle.lat]);
		}
	}


	createNewBusMarker(map, bus){
		return (new mapboxgl.Marker({color:'#0089C6'}))
			.setLngLat([bus.lng, bus.lat])
			.setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
				.setHTML(`<h3>${bus.name} (${bus.id})</h3><div>
				<p>${bus.date}</p>
				</div>`))
			.addTo(map);
	}

	render() {
		return (
			<template/>
		);
	}
}

Live.propTypes = {
	map: PropTypes.object,
	vehicles: PropTypes.array,
	updatedVehicle: PropTypes.object,
	congestion: PropTypes.object
};

export default (Live);
