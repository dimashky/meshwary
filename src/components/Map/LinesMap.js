import React from 'react';
import PropTypes from 'prop-types';
import swal from 'sweetalert2';
import '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css';
import mapboxgl from 'mapbox-gl';
import Utilities from '../../api/Utilities';
const MapboxDraw = require('@mapbox/mapbox-gl-draw');

class LinesMap extends React.Component {

	constructor(props){
		super(props);

		let map = this.props.map;

		this.edited_items = {};
		const draw = new MapboxDraw({
			userProperties: true,
			displayControlsDefault: false,
			controls: {
				line_string: true,
				trash: true
			},
			styles: Utilities.getMapboxDrawStyles()
		});

		this.state = {
			data: null,
			draw
		};

		map.addControl(draw, 'top-right');
		map.on('load', () => this.createLayers());
		map.on('draw.create', ({features}) => this.createNewLine(features[0]));
		map.on('draw.update', ({features}) => {this.edited_items = features[0];});
		map.on('draw.selectionchange', ({features}) => this.updateLine(features[0]));
		map.on('draw.delete', ({features}) => this.deleteLine(features[0]));
	}

	deleteLine(feature){
		let idx = this.props.lines.features.findIndex(e => e.id === feature.id);
		this.props.delete(feature.id);
	}

	async createNewLine(feature){

		const {value: lineName} = await swal.fire({
			title: 'Bus Line Name',
			input: 'text',
			allowOutsideClick: false,
			showCancelButton: false,
			inputValidator: (value) => {
				if (!value) {
					return 'You need to write something!';
				}
			}
		});

		if (!lineName) {
			return;
		}
		feature.properties['name']  = lineName;
		this.props.submit({name: lineName, feature});
	}

	updateLine(feature){
		if(feature){
			if(!this.edited_items.id || this.edited_items.id !== feature.id){
				this.edited_items = feature;
				this.forceUpdate();
			}
			return;
		}

		let idx = this.props.lines.features.findIndex(e => e.id === this.edited_items.id);
		if(idx === -1){
			return;
		}
		this.edited_items.properties['name'] = this.props.lines.features[idx].properties['name'];
		this.props.submit({feature: {...this.edited_items}}, this.edited_items.id);
		this.edited_items = {};
	}

	componentDidUpdate(prevProps){
		let {stops, lines, map} = this.props;
		let stopSource = map.getSource('stops');
		if(prevProps.lines !== lines && lines.features){
			this.state.draw.deleteAll();
			this.state.draw.add(lines);
			this.handleMarker();
		}
		if(prevProps.stops !== stops && stops.features && stopSource) {
			stopSource.setData(stops);
		}
	}

	handleMarker(){
		let {lines, map} = this.props;

		if(!lines.features || lines.features.length > 1){
			if(this.start_marker && this.end_marker){
				this.start_marker.remove();
				this.end_marker.remove();
			}
			return;
		}

		let line = lines.features[0];
		let start_point, end_point;

		if(line.geometry.coordinates.length === 1 && Array.isArray(line.geometry.coordinates[0])){
			start_point = line.geometry.coordinates[0][0];
			end_point = line.geometry.coordinates[0][line.geometry.coordinates[0].length - 1];
		}
		else{
			start_point = line.geometry.coordinates[0];
			end_point = line.geometry.coordinates[line.geometry.coordinates.length - 1];
		}

		this.start_marker.setLngLat(start_point).addTo(map);
		this.end_marker.setLngLat(end_point).addTo(map);
	}

	createLayers(){
		let {map, lines, stops} = this.props;

		this.start_marker = new mapboxgl.Marker({color: '#60D394'});
		this.end_marker = new mapboxgl.Marker({color: '#D33E43'});

		if(lines.features){
			this.state.draw.deleteAll();
			this.state.draw.add(lines);
			this.handleMarker();
		}

		if(!stops.feature){
			map.addSource('stops', {type: 'geojson', data: {
				'type': 'Feature',
				'geometry': {
					'type': 'Point',
					'coordinates': [0,0]
				},
				'properties': {}
			}});
		}
		else{
			map.addSource('stops', {type: 'geojson', data: stops});
		}


		map.addLayer({
			'id': 'stops',
			'type': 'fill',
			'source': 'stops',
			'paint': {
				'fill-color': '#454955',
				'fill-opacity': 0.3
			}
		});

		map.on('click', 'stops', function(e)  {
			new mapboxgl.Popup()
				.setLngLat(e.lngLat)
				.setHTML(e.features[0].properties.name)
				.addTo(map);
		});

		map.on('mouseenter', 'stops', () => {
			map.getCanvas().style.cursor = 'pointer';
		});

		map.on('mouseleave', 'stops',  () => {
			map.getCanvas().style.cursor = '';
		});
	}

	render() {
		let {lines} = this.props;
		let {edited_items} = this;

		if((!lines.features || lines.features.length > 1) && !edited_items.id) {
			return [];
		}

		let line_name = edited_items.id ? edited_items.properties['name'] : lines.features[0].properties['name'];
		return(<div className="map-infobox">{line_name}</div>);
	}
}

LinesMap.propTypes = {
	map: PropTypes.object,
	stops: PropTypes.object,
	lines: PropTypes.object,
	submit: PropTypes.func,
	delete: PropTypes.func
};

export default LinesMap;
