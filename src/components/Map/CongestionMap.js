import React from 'react';
import PropTypes from 'prop-types';
import Congestion from '../../api/Congestion';
import moment from 'moment';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import MenuItem from '@material-ui/core/MenuItem';

class CongestionMap extends React.Component {

	constructor(props){
		super(props);

		let weekday = moment().weekday() - 1, hour = moment().hour();
		let map = this.props.map;

		this.state = {
			hoveredFeature: null,
			data: null,
			weekday,
			hour
		};

		map.on('load', () => this.getCongestion(weekday, hour));
	}

	getCongestion(weekday, hour){
		let map = this.props.map;

		try{
			map.removeLayer('congestion');
			map.removeSource('congestion');
		}
		catch (e) {
			console.log(e);
		}

		Congestion.index(weekday, hour)
			.then(res => {
				map.addSource('congestion', {
					'type': 'geojson',
					'data': res
				});

				map.addLayer({
					'id': 'congestion',
					'type': 'line',
					'source': 'congestion',
					'paint': {
						'line-color':['get','color'],
						'line-width': 3
					}
				});
			})
			.catch(err => {
				alert(err.message);
			});
	}

	handleChangeWeekday(val){
		this.setState({weekday: val.target.value});
		this.getCongestion(val.target.value, this.state.hour);
	}

	handleChangeHour(val){
		this.setState({hour: val.target.value});
		this.getCongestion(this.state.weekday, val.target.value);
	}

	renderHours(){
		let jsx = [];
		for(let i = 1; i < 25; ++i){
			jsx.push(<MenuItem key={i} value={i}>{i}</MenuItem>);
		}
		return jsx;
	}

	render() {
		let {weekday, hour} = this.state;
		return (
			<div className="map-congestion-panel">
				<div>
					<InputLabel htmlFor="congestion-weekday">Weekday</InputLabel>&nbsp;:&nbsp;&nbsp;
					<Select
						value={weekday}
						onChange={this.handleChangeWeekday.bind(this)}
						input={<Input name="weekday" id="congestion-weekday" />}
					>
						<MenuItem value={0}>Monday</MenuItem>
						<MenuItem value={1}>Tuesday</MenuItem>
						<MenuItem value={2}>Wednesday</MenuItem>
						<MenuItem value={3}>Thursday</MenuItem>
						<MenuItem value={4}>Friday</MenuItem>
						<MenuItem value={5}>Saturday</MenuItem>
						<MenuItem value={6}>Sunday</MenuItem>
					</Select>
				</div>
				<div>
					<InputLabel htmlFor="congestion-hour">Hour</InputLabel>&nbsp;:&nbsp;&nbsp;
					<Select
						value={hour}
						onChange={this.handleChangeHour.bind(this)}
						input={<Input name="hour" id="congestion-hour" />}
					>
						{this.renderHours()}
					</Select>
				</div>
			</div>
		);
	}
}

CongestionMap.propTypes = {
	map: PropTypes.object
};

export default CongestionMap;
