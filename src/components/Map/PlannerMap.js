import React from 'react';
import PropTypes from 'prop-types';
import mapboxgl from 'mapbox-gl';
import randomColor from 'randomcolor';
import Congestion from "../../api/Congestion";
import moment from 'moment';
const MapboxDraw = require('@mapbox/mapbox-gl-draw');

class PlannerMap extends React.Component{
	constructor(props){
		super(props);
		this.markers = [];
		this.layers = [];
		this.state = {
			source: props.source,
			destination: props.destination,
			source_marker: {},
			destination_marker: {}
		};
		props.map.addControl(new MapboxDraw({
			displayControlsDefault: false,
		}), 'top-right');
	}

	componentDidMount(){

		let source_marker = (new mapboxgl.Marker({
			color: '#60D394',
			draggable: true
		}))
			.setLngLat(this.props.source)
			.setPopup(new mapboxgl.Popup({ offset: 25 })
				.setHTML('<h2>Source</h2>'))
			.addTo(this.props.map);

		let destination_marker = (new mapboxgl.Marker({
			color: '#D33E43',
			draggable: true
		}))
			.setLngLat(this.props.destination)
			.setPopup(new mapboxgl.Popup({ offset: 25 })
				.setHTML('<h2>Destination</h2>'))
			.addTo(this.props.map);

		source_marker.on('dragend',(marker) => this.onDragMarker(marker, 'source'));
		destination_marker.on('dragend',(marker) => this.onDragMarker(marker, 'destination'));

		this.setState({source_marker, destination_marker});
	}

	renderLines(lines){
		const map = this.props.map;
		try {
			map.removeLayer('lines');
			map.removeSource('lines');
		}
		catch (e) {
			console.log(e.message);
		}
		this.layers = Array(lines.length);

		let colors = randomColor({luminosity: 'light', count: lines.length});

		lines.forEach((line,i) => {
			line.properties.color = colors[i];
			line.properties.width = 6;
			if(line.properties.type === -1){
				line.properties.color = '#0089C6';
				line.properties.width = 4;
			}
		});

		map.addLayer({
			'id': 'lines',
			'type': 'line',
			'source': {
				'type': 'geojson',
				'data': {
					'type': 'FeatureCollection',
					'features': lines
				}
			},
			'layout': {
				'line-join': 'round',
				'line-cap': 'round'
			},
			'paint': {
				'line-color': ['get', 'color'],
				'line-width': ['get', 'width'],
				'line-opacity': 1
			}
		});

		map.on('click', 'lines', function (e) {
			var coordinates = e.features[0].geometry.coordinates.slice();
			var description = e.features[0].properties.name;
			try {
				new mapboxgl.Popup()
					.setLngLat(coordinates[Math.floor((coordinates.length)/2)])
					.setHTML(description)
					.addTo(map);
			}
			catch (e) {
				console.error(e);
			}
		});

		map.on('mouseenter', 'lines', function () {
			map.getCanvas().style.cursor = 'pointer';
		});

		// Change it back to a pointer when it leaves.
		map.on('mouseleave', 'lines', function () {
			map.getCanvas().style.cursor = '';
		});
	}

	renderStops(stops){
		const map = this.props.map;
		this.markers.forEach(marker => marker.remove());
		this.markers = Array(stops.length);
		stops.forEach((stop, idx) => {
			const el = document.createElement('div');
			el.className = 'bus-stop-marker';
			this.markers[idx] = new mapboxgl.Marker(el)
				.setLngLat([stop.lng, stop.lat])
				.setPopup(new mapboxgl.Popup().setText(stop.name))
				.addTo(map);
		});
	}

	onDragMarker(marker, name){
		let lngLat = marker.target.getLngLat();
		this.props.onDragMarker(name === 'source', lngLat.lat, lngLat.lng);
	}

	handleCongestionUpdate(show){
		let map = this.props.map;

		try{
			map.removeLayer('congestion');
			map.removeSource('congestion');
		}
		catch (e) {
			console.log(e.message);
		}

		if(!show){
			return;
		}
		const now = moment("2019-06-30 13:00:00");
		const weekday = now.weekday(),
			hour = now.hour();

		Congestion.index(weekday, hour)
			.then(res => {
				map.addSource('congestion', {
					'type': 'geojson',
					'data': res
				});

				map.addLayer({
					'id': 'congestion',
					'type': 'line',
					'source': 'congestion',
					'paint': {
						'line-color':['get','color'],
						'line-width': 2,
						'line-opacity': 0.2
					}
				});
			})
			.catch(err => {
				alert(err.message);
			});
	}

	componentDidUpdate(prevProps){
		let {lines, stops} = this.props;
		if(lines && stops && prevProps.lines && prevProps.stops && (lines.length !== prevProps.lines.length || stops.length !== prevProps.stops.length)){
			this.renderLines(lines);
			this.renderStops(stops);
		}

		if(prevProps.congestion !== this.props.congestion){
			this.handleCongestionUpdate(this.props.congestion);
		}
	}

	render(){
		return [];
	}
}

PlannerMap.propTypes = {
	map: PropTypes.object,
	source: PropTypes.array,
	destination: PropTypes.array,
	onDragMarker: PropTypes.func.isRequired,
	stops: PropTypes.array,
	lines: PropTypes.array,
	congestion: PropTypes.bool
};

export default PlannerMap;
