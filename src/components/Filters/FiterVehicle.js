import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import BusIcon from '../../assets/images/icons/Bus.js';
import HippieIcon from '../../assets/images/icons/HippieVan.js';
import TaxiIcon from '../../assets/images/icons/FrontalTaxiCab';

class FilterVehicle extends Component{
	constructor(props){
		super(props);
		this.state = {
			showBus: true,
			showMicroBus: false,
			showTaxi: false
		};
	}

	changeType(type) {
		let tmp = {};
		tmp[type] = !this.state[type];
		this.setState({...this.state, ...tmp});
	}

	render() {

		return(
			<Grid   
				className="filter-vehicle-container"
				container
				direction="row"
				justify="space-around"
				alignItems="baseline"
			>
				<div className="vehicle-type-icon">
					<a href="javascript:void(0)" onClick={() => {return; this.changeType('showBus');}}>
						<BusIcon className={(this.state.showBus ? 'type-active' : '')}></BusIcon>
					</a>
				</div>
				<div className="vehicle-type-icon">
					<a style={{cursor: 'not-allowed'}} onClick={() => {return; this.changeType('showMicroBus');}}>
						<HippieIcon className={(this.state.showMicroBus? 'type-active' : '')}></HippieIcon>
					</a>
				</div>
				<div className="vehicle-type-icon">
					<a style={{cursor: 'not-allowed'}} onClick={() => {return; this.changeType('showTaxi');}}>
						<TaxiIcon className={this.state.showTaxi ? 'type-active' : ''}></TaxiIcon>
					</a>
				</div>
			</Grid>
		);
	}
}


export default FilterVehicle;