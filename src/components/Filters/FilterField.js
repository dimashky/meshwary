import React, {Component} from 'react';

import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Search from '@material-ui/icons/Search';
import PropTypes from 'prop-types';

class FilterField extends Component{

	handleChange(e){
		if(this.props.handleSearch){
			this.props.handleSearch(e.target.value);
		}
	}

	render() {
		return(
			<div className="filter-field-container">
				<TextField
					className="filter-field-input"
					fullWidth={false}
					onChange={this.handleChange.bind(this)}
					InputProps={{
						disabled: false,
						placeholder: 'Search ....',
						dir: 'auto',
						startAdornment: (
							<InputAdornment position="start" dir="auto">
								<Search/>
							</InputAdornment>
						),
					}}
				/>
			</div>
		);
	}
}

FilterField.propTypes = {
	handleSearch: PropTypes.func
};

export default FilterField;
