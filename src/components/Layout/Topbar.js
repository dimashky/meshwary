import React from 'react';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import NotificationsNone from '@material-ui/icons/NotificationsNone';
import Settings from '@material-ui/icons/Settings';
import ExitToApp from '@material-ui/icons/ExitToApp';
import Fullscreen from '@material-ui/icons/Fullscreen';
import screenfull from 'screenfull';
import Swal from 'sweetalert2';
import Cookies from 'js-cookie';

import { NavLink  as RouteLink } from 'react-router-dom';
import User from '../../api/User';

class Topbar extends React.Component{
	async logout(){
		let {value:confirm} = Swal.fire({
			title: 'Logout',
			text: 'Are you sure to logout from this account?',
			showCancelButton: true,
			showLoaderOnConfirm: true,
			preConfirm: () => {
				return new Promise((resolve) => {
					Cookies.remove('access_token');
					setTimeout(() => {
						window.location.replace('/');
						resolve();
					}, 1000);
				});
			}
		});
	}

	toggleFullscreen(){
		if (screenfull.enabled) {
			screenfull.toggle();
		}
	}

	render(){
		return(
			<div className="topbar-container">
				<Grid
					className=""
					container
				>
					<Grid item xs={2}>
						<div className="topbar-title">Meshwary</div>
					</Grid>
					<Grid item xs={7}>
						<div className="topbar-router-container">
							<div className="topbar-router-link">
								<RouteLink exact={true} activeClassName="active" to="/">Tracker</RouteLink>
							</div>
							<div className="topbar-router-link">
								<RouteLink activeClassName="active" to="/planner">Planner</RouteLink>
							</div>
							<div className="topbar-router-link">
								<RouteLink activeClassName="active" to="/congestion">Congestion</RouteLink>
							</div>
							<div className="topbar-router-link">
								<RouteLink activeClassName="active" to="/stations">Stops</RouteLink>
							</div>
							<div className="topbar-router-link">
								<RouteLink activeClassName="active" to="/lines">Lines</RouteLink>
							</div>
							<div className="topbar-router-link">
								<RouteLink activeClassName="active" to="/correlation">Correlation</RouteLink>
							</div>
						</div>
					</Grid>
					<Grid item xs={3}>
						<div className="links">
							<Grid
								container
								direction="row"
								justify="flex-end"
								alignItems="baseline">
								<Grid item>
									<a href="javascript:void(0)" onClick={this.toggleFullscreen} className="notification-icon">
										<Fullscreen style={{color: 'white'}}/>
									</a>
								</Grid>
								{false &&
									<Grid item>
										<Link href="#setting" color="inherit">
											<Settings style={{color: 'white'}}/>
										</Link>
									</Grid>
								}
								<Grid item>
									<Link href="#" onClick={() => this.logout()} color="inherit">
										<ExitToApp style={{color: 'white'}}/>
									</Link>
								</Grid>
							</Grid>
						</div>
					</Grid>
				</Grid>
			</div>
		);
	}
}


export default Topbar;
