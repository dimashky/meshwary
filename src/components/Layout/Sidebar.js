import React from 'react';
import { Link } from 'react-router-dom';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';

import Tooltip from '@material-ui/core/Tooltip';

class Sidebar extends React.Component{
	render(){
		return(
			<div className="sidebar-container">

				<List component="nav" className="sidebar-actions">
					<ListItem>
						<ListItemIcon>
							<Tooltip title="Tracking Map" placement="right">
								<Link to="/"><img className="sidebar-icon" src={require('../../assets/images/icons/satellite-signal.svg')} alt="live"></img></Link>
							</Tooltip>
						</ListItemIcon>
					</ListItem>
					<ListItem>
						<ListItemIcon>
							<Tooltip title="Stations" placement="right">
								<Link to="/stations"><img className="sidebar-icon" src={require('../../assets/images/icons/bus-stop.svg')} alt="stops"></img></Link>
							</Tooltip>
						</ListItemIcon>
					</ListItem>
					<ListItem>
						<ListItemIcon>
							<Tooltip title="Commuters" placement="right">
								<Link to="/commuters"><img className="sidebar-icon" src={require('../../assets/images/icons/commuter.svg')} alt="live"></img></Link>
							</Tooltip>
						</ListItemIcon>
					</ListItem>
				</List>
			</div>
		);
	}
}


export default Sidebar;
