import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import NavigationIcon from '@material-ui/icons/NearMe';
import Fab from '@material-ui/core/Fab/Fab';
import {getGeoName} from '../../api/GIS';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import { blue, blueGrey } from '@material-ui/core/colors';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import DirectionsBus from '@material-ui/icons/DirectionsBus';
import DirectionsRun from '@material-ui/icons/DirectionsRun';
import DirectionsCar from '@material-ui/icons/DirectionsCar';
import Checkbox from '@material-ui/core/Checkbox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import TrafficIcon from '@material-ui/icons/TrafficSharp';
import TrafficBorderIcon from '@material-ui/icons/TrafficOutlined';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const BlueCheckbox = withStyles({
	root: {
		color: blueGrey[400],
		'&$checked': {
			color: blue[400],
		},
	},
	checked: {},
})(props => <Checkbox color="default" {...props} />);

class TripInput extends React.Component{
	constructor(props){
		super(props);

		this.state = {
			congestion: false,
			source: props.source,
			destination: props.destination,
			sourceName: props.source,
			destinationName: props.destination,
			type: 'bus'
		};
		this.updateName(props.source, false);
		this.updateName(props.destination, true);
	}

	componentDidUpdate(prevProps){
		let currentProps = this.props;
		if(prevProps.source !== currentProps.source || prevProps.destination !== currentProps.destination){
			let source= currentProps.source,
				destination= currentProps.destination;
			this.setState({source, destination});
			this.updateName(source, false);
			this.updateName(destination, true);
		}

	}

	handleChange(event){
		const { target: { name, value } } = event;
		this.setState({[name]:value});
		this.updateName(value, name === 'destination');
	}

	updateName(latLngStr, isDestination){
		const latLng = latLngStr.split(',');
		const name = isDestination ? 'destinationName' : 'sourceName';
		if(latLng.length !== 2){
			this.setState({[name]:latLngStr});
			return;
		}
		getGeoName(parseFloat(latLng[1]), parseFloat(latLng[0]))
			.then(res => {
				this.setState({[name]:res});
			})
			.catch(err => {
				this.setState({[name]:latLngStr});
				console.log(err.message);
			});
	}

	handleTypeChange(newType){
		this.setState({type: newType});
	}

	render() {
		let {source, destination, sourceName, destinationName, type, congestion} = this.state;
		return (
			<div className="planner-input-container">
				<TextField
					label="Source"
					name="source"
					style={{ margin: 8 }}
					placeholder="Start Point you will go!"
					fullWidth
					margin="normal"
					variant="outlined"
					value={sourceName}
					InputLabelProps={{
						shrink: true,
					}}
					onChange={this.handleChange.bind(this)}
				/>
				<TextField
					label="Destination"
					name="destination"
					style={{ margin: 8 }}
					placeholder="Your Target Point!"
					fullWidth
					margin="normal"
					value={destinationName}
					variant="outlined"
					InputLabelProps={{
						shrink: true,
					}}
					onChange={this.handleChange.bind(this)}
				/>
				<div className="planner-submit">
					<Grid container spacing={16} justify="center" alignItems="center" direction="row">
						<Grid item>
							<ToggleButtonGroup >
								<Button color={type === 'bus' ? 'primary' : 'secondary'} onClick={() => this.handleTypeChange('bus')}>
									<DirectionsBus />
								</Button>
								<Button color={type === 'car' ? 'primary' : 'secondary'} onClick={() => this.handleTypeChange('car')}>
									<DirectionsCar />
								</Button>
								<Button color={type === 'run' ? 'primary' : 'secondary'} onClick={() => this.handleTypeChange('run')}>
									<DirectionsRun />
								</Button>
							</ToggleButtonGroup>
						</Grid>
						<Grid item>
							<FormControlLabel
								control={
									<BlueCheckbox icon={<TrafficBorderIcon size="small"/>} checkedIcon={<TrafficIcon size="small"/>}
		                checked={this.state.congestion}
		                onChange={(event) => this.setState({ 'congestion': event.target.checked })}
									/>
								}
								label="Enable Congestion?"
							/>
						</Grid>
						<Grid item>
							<Button style={{borderRadius:0}} onClick={() => this.props.submitHandler(source, destination, type, congestion)} variant="contained" fullWidth={true} size="small" aria-label="Go" color="primary">
								Go <NavigationIcon />
							</Button>
						</Grid>
					</Grid>
				</div>
			</div>
		);
	}
}

TripInput.propTypes = {
	submitHandler: PropTypes.func.isRequired,
	source: PropTypes.string.isRequired,
	destination: PropTypes.string.isRequired
};

export default TripInput;
