import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.section`
	display: block;
	position: absolute;
	top: 0;
	left: 0;
	z-index: 100;
	width: 100%;
	height: 100%;
	background: rgba(69, 73, 85, 0.99) url("/loading2.gif") no-repeat center;
`;

class Loader  extends React.Component {
	render() {
		return (
			<Wrapper/>
		);
	}
}

export default Loader;
