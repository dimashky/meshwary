import React, { Component } from 'react';
import ReactMapGL, {NavigationControl} from 'react-map-gl';
import { setRTLTextPlugin } from 'mapbox-gl';
import StopsMap from './Map/StopsMap';
import LinesMap from './Map/LinesMap';
import LiveMap from './Map/Live';
import CongestionMap from './Map/CongestionMap';
import CorrelationMap from './Map/CorrelationMap';
import mapboxgl from 'mapbox-gl';
import PropTypes from 'prop-types';
import PlannerMap from './Map/PlannerMap';

const MapboxGeocoder = require('@mapbox/mapbox-gl-geocoder');
require('@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css');

const MapboxLanguage = require('@mapbox/mapbox-gl-language');

setRTLTextPlugin('https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-rtl-text/v0.2.1/mapbox-gl-rtl-text.js');

class Map extends Component {

	constructor(props){
		super(props);

		this.mapRef = React.createRef();

		let longitude = (localStorage.getItem('longitude') ? parseFloat(localStorage.getItem('longitude')) : 36.2765),
			latitude = (localStorage.getItem('latitude') ? parseFloat(localStorage.getItem('latitude')) : 33.5138),
			zoom = (localStorage.getItem('zoom') ? parseInt(localStorage.getItem('zoom')) : 13);
		if(!zoom){
			zoom = 14;
		}

		let map_style = 'mapbox://styles/mapbox/streets-v9', type = this.props.type;

		if(type === 'commuters' || type === 'correlation' || type === 'live' || type === 'planner'){
			map_style = 'mapbox://styles/mapbox/dark-v9';
		}

		this.state = {
			viewport: {
				latitude,
				longitude,
				zoom
			},
			draggable: false,
			map: {},
			map_style,
			mapbox_token: 'pk.eyJ1IjoiZGltYXNoa3k5NiIsImEiOiJjazF1Y20yeXAwNTI1M2dtd3o0c3FxMnpzIn0.yiPFVUkhY2i70C0Yi_901A'
		};
	}


	componentDidUpdate(prevProps){
		if(prevProps.mapCenter !== this.props.mapCenter && this.props.mapCenter){
			this._updateViewport({
				latitude: this.props.mapCenter.lat,
				longitude: this.props.mapCenter.lng,
				zoom: (this.props.mapCenter.zoom ? this.props.mapCenter.zoom : this.state.viewport.zoom)
			});
		}
	}

	_updateViewport(viewport) {
		localStorage.setItem('latitude', viewport.latitude ? viewport.latitude : 33.5138);
		localStorage.setItem('longitude', viewport.longitude ? viewport.longitude : 36.2765);
		localStorage.setItem('zoom', viewport.zoom);

		this.setState({viewport});
	}


	componentDidMount() {
		let map = this.mapRef.current.getMap();
		this.setState({map});
		map.dragPan.enable();
		map.on('moveend', () => {
			const center = map.getCenter();
			this._updateViewport({latitude: center.lat, longitude: center.lng, zoom: map.getZoom()});
		});
		this._addLanguageControllerToMap();
		this._addGeocoderControllerToMap();
		map.addControl(new mapboxgl.FullscreenControl());
	}

	_handleChangeStyleEvent(style){
		if(!style){
			style = 'mapbox://styles/mapbox/streets-v9';
		}
		this.setState({map_style: style});
	}

	_addLanguageControllerToMap(){
		let map = this.mapRef.current.getMap();

		let language = new MapboxLanguage({
			defaultLanguage: 'ar'
		});
		map.addControl(language);
	}

	_addGeocoderControllerToMap(){
		let map = this.mapRef.current.getMap();

		map.addControl(new MapboxGeocoder({
			accessToken: this.state.mapbox_token,
			countries: 'sy',
		}));
	}

	_renderChildContent(){
		let jsx = <div/>;

		switch (this.props.type) {
		case 'stops':
			jsx = <StopsMap map={this.state.map}
				{...this.props}/>;
			break;
		case 'lines':
			jsx = <LinesMap map={this.state.map}
				{...this.props}/>;
			break;
		case 'live':
			jsx = <LiveMap map={this.state.map}
				{...this.props}/>;
			break;
		case 'commuters':
			jsx = <CongestionMap map={this.state.map}{...this.props}/>;
			break;
		case 'correlation':
			jsx = <CorrelationMap map={this.state.map}{...this.props}/>;
			break;
		case 'planner':
			jsx = <PlannerMap map={this.state.map}
				{...this.props}/>;
			break;
		default:
			break;
		}
		return jsx;
	}

	toggleDrag(){
		this.setState({draggable: !this.state.draggable});
	}

	render() {
		const {viewport, map_style, draggable} = this.state;
		return (
			<div className="map-container">
				<ReactMapGL
					ref={this.mapRef}
					mapStyle={map_style}
					{...viewport}
					width="100%"
					reuseMaps={false}
					height="100%"
					dragPan={false}
					mapboxApiAccessToken={this.state.mapbox_token}
					onViewportChange={this._updateViewport.bind(this)}
				>

					{this._renderChildContent()}


					<div className="map-nav">
						<NavigationControl onViewportChange={this._updateViewport.bind(this)} />
					</div>


				</ReactMapGL>

			</div>
		);
	}
}

Map.propTypes = {
	type: PropTypes.string.isRequired,
	mapCenter: PropTypes.object
};

export default Map;
