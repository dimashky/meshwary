import * as TYPES from './actionTypes';

export const UPDATE_PAGE_TITLE = (title) => {
	return{
		type: TYPES.EDIT_PAGE_TITLE,
		pageTitle: title
	};
};

export const TURN_OFF_LOADING_INDICATOR = () => {
	return{
		type: TYPES.TURN_OFF_PAGE_LOADING
	};
};

export const TURN_ON_LOADING_INDICATOR = () => {
	return{
		type: TYPES.TURN_ON_PAGE_LOADING
	};
};

export const CONNECT_TO_SOCKET = (socket) => {
	return{
		type: TYPES.CONNECT_TO_SOCKET,
		socket: socket
	};
};