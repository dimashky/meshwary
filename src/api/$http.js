import axios from 'axios';
import Cookies from 'js-cookie';

let server_address = '';

if (process.env.NODE_ENV === 'development') {
	server_address = 'http://localhost:8000';
}

axios.defaults.baseURL = `${server_address}/api/`;
axios.defaults.headers.Accept = 'application/json';

const access_token = Cookies.get('access_token');
axios.defaults.headers.Authorization = `Bearer ${access_token}`;

export default axios;
