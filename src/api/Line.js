import $http from './$http';
import randomColor from 'randomcolor';

class Line {
	static async index(){
		let lines = (await $http.get('static_line')).data.data;
		lines.features = lines.features.filter(e => e.geometry.coordinates.length > 1);
		let colors = randomColor({luminosity: 'dark', count: lines.features.length});
		lines.features = lines.features.map((e,i) => {
			e.id = e._id;
			e.properties.portColor = colors[i];
			return e;
		});
		return lines;
	}

	static async create(data){
		let new_line = (await $http.post('static_line', data)).data.data;
		new_line.id = new_line._id;
		return new_line;
	}

	static async update(id, data){
		return (await $http.put('static_line/'+id, data)).data.data;
	}

	static async delete(id){
		return (await $http.delete('static_line/'+id)).data.data;
	}
}

export default Line;
