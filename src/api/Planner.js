import $http from './$http';

class Planner {
	static async findPath(source, destination, type, congestion){
		let res = (await $http.get('/rp?source_lat='+source[1]+
			'&source_lng='+source[0]+
			'&destination_lat='+destination[1]+
			'&destination_lng='+destination[0]+
			'&type='+type+'&congestion='+congestion)).data;
		return res;
	}
}

export default Planner;
