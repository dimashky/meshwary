import axios from 'axios';

export default class Congestion{
	static async index(requestedWeekday, requestedHour) {
		return (await axios.get('/congestion/' + requestedWeekday + '/' + requestedHour)).data.data;
	}

	static async correlatedWays(way_id=-1) {
		if(way_id === -1){
			return (await axios.get('/correlation/ways')).data.data;
		}
		else {
			return (await axios.get(`/correlation/${way_id}`)).data.data;
		}
	}
}
