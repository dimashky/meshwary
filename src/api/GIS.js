import axios from 'axios';

export async function getGeoName(lat, lng) {
	let res = (await axios.get(`https://nominatim.openstreetmap.org/reverse?lat=${lat}&lon=${lng}&format=json&zoom=16`)).data;
	let names = res['display_name'].split(',');
	if(names.length > 0){
		return names[0];
	}
	return res['display_name'];
}
