import $http from './$http';

class Stop {
	static async index(){
		let stops = ((await $http.get('stop')).data.data);
		stops.features = stops.features.filter(e => e.geometry.coordinates.length >= 4 || e.geometry.coordinates.length === 1);
		stops.features = stops.features.map(e => {
			e.id = e._id;
			if(e.geometry.coordinates.length > 1){
				e.geometry.coordinates.push(e.geometry.coordinates[0]);
				e.geometry.coordinates = [e.geometry.coordinates];
			}
			return e;
		});
		return stops;
	}

	static async create(data){
		let new_stop = (await $http.post('stop', data)).data.data;
		new_stop.id = new_stop._id;
		if(new_stop.geometry.coordinates.length > 2){
			new_stop.geometry.coordinates = [new_stop.geometry.coordinates];
		}
		return new_stop;
	}

	static async update(id, data){
		return (await $http.put('stop/'+id, data)).data.data;
	}

	static async delete(id){
		return (await $http.delete('stop/'+id)).data.data;
	}
}

export default Stop;
