import $http from './$http';
import axios from "axios";
import Cookies from 'js-cookie';

class User {
	static current_user = {};

	static async checkAccessToken(access_token){
		$http.defaults.headers.Authorization = `Bearer ${access_token}`;
		try{
			return (await $http.get('user')).data;
		}
		catch (e) {
			return false;
		}
	}

	static async login(email, password){
		return (await $http.post('login', {email, password})).data.data;
	}

	static setUser(user, access_token = null){
		this.current_user = user;
		if(!user.accessToken && access_token)
			this.current_user.accessToken = access_token;
		if(access_token){
			$http.defaults.headers.Authorization = `Bearer ${access_token}`;
			Cookies.set('access_token', access_token, { expires: 365 });
		}
	}

	static  getUser(){
		return this.current_user;
	}

	static async logout(){
		return (await $http.post('logout')).data.data;
	}
}

export default User;
