import * as TYPES from '../actions/actionTypes';

const initialState = {
	pageTitle: 'Meshwary',
	isLoading: false
};

function rootReducer(state = initialState, action) {
	let new_state = Object.assign({}, state);
	switch (action.type) {
	case TYPES.EDIT_PAGE_TITLE:
		new_state = Object.assign(new_state, {pageTitle: action.pageTitle});
		break;
	case TYPES.TURN_OFF_PAGE_LOADING:
		new_state = Object.assign(new_state, {isLoading: false});
		break;
	case TYPES.TURN_ON_PAGE_LOADING:
		new_state = Object.assign(new_state, {isLoading: true});
		break;
	case TYPES.CONNECT_TO_SOCKET:
		new_state = Object.assign(new_state, {socket: action.socket});
		break;
	default:
		break;
	}
	return new_state;
}

export default rootReducer;
